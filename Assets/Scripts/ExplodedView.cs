﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class SubMeshes
{
    public GameObject gameObject;
    public MeshRenderer meshRenderer;
    public Vector3 originalPosition;
    public Vector3 explodedPosition;
}

public class ExplodedView : MonoBehaviour
{
    #region Variables

    public List<SubMeshes> childMeshRenderers;
    bool isInExplodedView = false;
    public float explodedSpeed = 0.1f;
    public float explodedMaxDistance = 2f;
    public float distance = 0.001f;
    public float distanceBetweenObjects = 2f;
    public float doubleDistance = 0.01f;
    bool isMoving = false;

    #endregion

    #region UnityFunctions

    private void Awake()
    {
        childMeshRenderers = new List<SubMeshes>();

        foreach (var item in GetComponentsInChildren<MeshRenderer>())
        {
            SubMeshes mesh = new SubMeshes();
            mesh.meshRenderer = item;
            mesh.originalPosition = item.transform.position;
            mesh.explodedPosition = item.bounds.center * explodedMaxDistance;
            childMeshRenderers.Add(mesh);
            //print("Name of item is: " + item.name);
        }
    }

    private void Update()
    {
        if (isInExplodedView)
        {
            SubMeshes prev = null;
            foreach (var item in childMeshRenderers)
            {
                item.meshRenderer.transform.position = Vector3.Lerp(item.meshRenderer.transform.position, item.explodedPosition, explodedSpeed);

                //print("Distance between " + item.meshRenderer.name + " original position and exploded position is: "
                //       + Vector3.Distance(item.meshRenderer.transform.position, item.explodedPosition).ToString());

                if (prev != null)
                {
                    print("Previous: " + prev.meshRenderer.name + " - Current: " + item.meshRenderer.name + " - Have a distance between them of: " 
                        + Vector3.Distance(prev.meshRenderer.transform.position, item.meshRenderer.transform.position).ToString());
                    if(Vector3.Distance(prev.meshRenderer.transform.position, item.meshRenderer.transform.position) < distanceBetweenObjects)
                    {
                        /*Vector3 directionToMove = item.explodedPosition - item.originalPosition;
                        directionToMove = directionToMove.normalized * Time.deltaTime * explodedSpeed;
                        float maxDistance = Vector3.Distance(item.originalPosition, item.explodedPosition);*/
                        item.meshRenderer.transform.position = Vector3.Lerp(item.meshRenderer.transform.position, item.explodedPosition, explodedSpeed);
                    }
                }

                prev = item;

            }
        }
        else
        {
            foreach (var item in childMeshRenderers)
            {
                item.meshRenderer.transform.position = Vector3.Lerp(item.meshRenderer.transform.position, item.originalPosition, explodedSpeed);

                /*while (Vector3.Distance(item.meshRenderer.transform.position, item.originalPosition) > doubleDistance)
                {
                    item.originalPosition = item.meshRenderer.bounds.center / explodedMaxDistance;
                    item.meshRenderer.transform.position = Vector3.Lerp(item.meshRenderer.transform.position, item.originalPosition, explodedSpeed);
                }*/

                   
            }
        }
    }

    #endregion

    #region CustomFunctions

    public void ToggleExplodedView()
    {
        if (isInExplodedView)
        {
            isInExplodedView = false;
            isMoving = true;
        }
        else
        {
            isInExplodedView = true;
            isMoving = true;
        }
    }

    #endregion

}